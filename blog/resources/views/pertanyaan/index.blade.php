@extends('adminlte.master')

@section('content')
          <div>
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                  <div class="alert alert-success">
                  {{ session('success') }}
                  </div>
                @endif
                <a class="btn btn-primary" href="/pertanyaan/create">Create a New Question</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">Id</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th>Tanggal Dibuat</th>
                      <th>Tanggal Diperbaharui</th>
                      <th>Profil Id</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pertanyaans as $key =>$pertanyaan)
                    <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $pertanyaan->judul }}</td>
                    <td>{{ $pertanyaan->isi }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="display:flex;">
                      <a href="/pertanyaan/{{ $pertanyaan->id }}" class="btn btn-info btn-sm">show</a>
                      <a href="/pertanyaan/{{ $pertanyaan->id }}/edit" class="btn btn-default btn-sm">edit</a>
                      <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                          @csrf 
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                      </form>
                    </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
@endsection